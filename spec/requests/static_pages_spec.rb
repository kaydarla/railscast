require 'spec_helper'

describe "StaticPages" do
  describe "Home page" do
    describe "GET /static_pages/home" do
      it "should have the content 'Sammper'" do
        visit '/static_pages/home'
        expect(page).to have_content('Sampper')
      end
      it "should have the right title" do
        visit '/static_pages/home'
        expect(page).to have_title("Sampper App | Home")
      end
    end
  end
  describe "Home page" do
    it "should have the content 'Sampper Application'" do
      visit '/static_pages/home'
      expect(page).to have_content('Sampper Application')
    end
  end
  describe "Help page" do
    it "should have the content 'Help'" do
      visit '/static_pages/help'
      expect(page).to have_content('Help')
    end
  end
  describe "About page" do
    it "should have the content 'About'" do
      visit '/static_pages/about'
      expect(page).to have_content('About')
    end
  end
end
